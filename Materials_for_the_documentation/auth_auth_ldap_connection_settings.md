---
title: Connection Settings
html_title: LDAP Connection
slug: active-directory-ldap-connection
category: ldap
order: 100
layout: docs
edition: enterprise
description: How to protect your Elasticsearch cluster by connecting to an LDAP or Active Directory server.
resources:
  - "search-guard-presentations#active-directory-ldap|LDAP & Active Directory configuration (presentation)"

---
<!---
Copyright 2019 floragunn GmbH
-->



# Active Directory and LDAP
{: .no_toc}

{% include toc.md %}

## Directory Service and LDAP 
A Directory Service is a central repository, a system in which information such as user profiles, access privilages, devices is stored. It is easy to look up information in the Directory Service as the information is stored in an organized manner. Lightweight Directory Access Protocol (LDAP) on the other hand is a protocol, which enables the client application to exchange infromation with the Directory Server. Active Directory is a type of Directory Server developed by Microsoft. The Active Directory's primary task is to manage the permissionto various resources stored in it.

![](./ActiveDirectory/Asset_1.jpg)

## LDAP and Active Directory in SearchGuard

In order to set up LDAP and Active Directory in SearchGuard you need to take two steps: 

1. Activate the module

2. Set the connections 

![](./ActiveDirectory/Asset_2.jpg)

The `authc` section is used for configuring authentication, which means to check if the user has entered the correct credentials. The `authz` is used for authorisation, which defines how the role(s) for an authenticated user are retrieved and mapped.

In most cases, you want to configure both authentication and authorization, however, it is also possible to use authentication only and map the users retrieved from LDAP directly to Search Guard roles. 

To enable LDAP authentication and authorization, add the following lines to sg_config.yml

**Authentication:**

```yaml
authc:
  ldap:
    enabled: true
    order: 1
    http_authenticator:
      type: basic
      challenge: false
    authentication_backend:
      type: ldap
      config:
        ...
```

**Authorization:**

```yaml
authz:
  ldap:
    enabled: true
  authorization_backend:
    type: ldap
    config:
      ...
```
![](./ActiveDirectory/Asset_3.jpg)

## Connection settings

The connection settings are identical for authentication and authorisation and are added to the `config` section of the respective domains.  

### Hostname and Port

Configure the hostname and port of your Active Directory server(s) like:

```yaml
config:
  hosts:
    - primary.ldap.example.com:389
    - secondary.ldap.example.com:389
```
  
You can configure more than one servers here. If Search Guard cannot connect to the first server, the second one is tried and so on.   

![](./ActiveDirectory/Asset_4.jpg)

### Bind DN and password

Configure the bind dn and password Search Guard uses when issuing queries to your Active Directory / LDAP server like:

```yaml
config:
  bind_dn: cn=admin,dc=example,dc=com
  password: password
```

These are basically the credential you are using to authenticate against your server. If your server supports anonymous authentication both `bind_dn` and `password` can be set to `null`. 


![](./ActiveDirectory/Asset_5.jpg)


### TLS settings

Use the following parameters to configure TLS for connecting to your server:

```yaml
config:
  enable_ssl: <true|false>
  enable_start_tls: <true|false>
  enable_ssl_client_auth: <true|false>
  verify_hostnames: <true|false>
```

![](./ActiveDirectory/Asset_6.jpg)

### Certificate validation

By default Search Guard validates the TLS certificate of the LDAP server(s) against the Root CA configured in elasticsearch.yml, either as PEM certificate or a truststore:

```
searchguard.ssl.transport.pemtrustedcas_filepath: ...
searchguard.ssl.http.truststore_filepath: ...
```

If your server uses a certificate signed by a different CA, import this CA to your truststore or add it to your trusted CA file on each node. 

You can also use a separate root CA in PEM format by setting **one of** the following configuration options:

```yaml
config:
  pemtrustedcas_filepath: /path/to/trusted_cas.pem
```

or

```yaml
config:
  pemtrustedcas_content: |-
    MIID/jCCAuagAwIBAgIBATANBgkqhkiG9w0BAQUFADCBjzETMBEGCgmSJomT8ixk
    ARkWA2NvbTEXMBUGCgmSJomT8ixkARkWB2V4YW1wbGUxGTAXBgNVBAoMEEV4YW1w
    bGUgQ29tIEluYy4xITAfBgNVBAsMGEV4YW1wbGUgQ29tIEluYy4gUm9vdCBDQTEh
    ...
```

![](./ActiveDirectory/Asset_7.jpg)

### Client authentication

If you use TLS client authentication, Search Guard sends the PEM certificate of the node, as configured in `elasticsearch.yml`.

by setting **one of** the following configuration options::

```yaml
config:
  pemkey_filepath: /path/to/private.key.pem
  pemkey_password: private_key_password
  pemcert_filepath: /path/to/certificate.pem
```

or

```yaml
config:
  pemkey_content: |-
    MIID2jCCAsKgAwIBAgIBBTANBgkqhkiG9w0BAQUFADCBlTETMBEGCgmSJomT8ixk
    ARkWA2NvbTEXMBUGCgmSJomT8ixkARkWB2V4YW1wbGUxGTAXBgNVBAoMEEV4YW1w
    bGUgQ29tIEluYy4xJDAiBgNVBAsMG0V4YW1wbGUgQ29tIEluYy4gU2lnbmluZyBD
    ...
  pemkey_password: private_key_password
  pemcert_content: |-
    MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCHRZwzwGlP2FvL
    oEzNeDu2XnOF+ram7rWPT6fxI+JJr3SDz1mSzixTeHq82P5A7RLdMULfQFMfQPfr
    WXgB4qfisuDSt+CPocZRfUqqhGlMG2l8LgJMr58tn0AHvauvNTeiGlyXy0ShxHbD
    ...
```

![](./ActiveDirectory/Asset_8.jpg)

### Enabled ciphers and protocols

You can limit the allowed ciphers and TLS protocols for the LDAP connection. For example, you can only allow strong ciphers and limit the TLS versions to the most recent ones.

```yaml
ldap:
  enabled: true
  ...
  authentication_backend:
    type: ldap
    config:
      enabled_ssl_ciphers:
        - "TLS_DHE_RSA_WITH_AES_256_CBC_SHA"
        - "TLS_DHE_DSS_WITH_AES_128_CBC_SHA256"
      enabled_ssl_protocols:
        - "TLSv1.1"
        - "TLSv1.2"
```

![](./ActiveDirectory/Asset_9.jpg)


By default Search Guard disables `TLSv1` because it is unsecure. 
{: .note .js-note .note-warning}

If you need to use `TLSv1` and you know what you  are doing, you can re-enable it like:

```yaml
enabled_ssl_protocols:
  - "TLSv1"
  - "TLSv1.1"
  - "TLSv1.2"
```