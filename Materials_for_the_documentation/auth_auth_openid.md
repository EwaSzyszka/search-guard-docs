---
title: OpenID Connect
html_title: OpenID Connect
slug: openid-json-web-keys
category: authauth
order: 600
layout: docs
edition: enterprise
description: How to connect Search Guard with an OpenID provider like Keycloak and how to use JSON web keys to automatially fetch encryption keys
resources:
  - "https://search-guard.com/kibana-openid-keycloak/|Kibana Single Sign-On with OpenID and Keycloak"

---
<!---
Copyright 2019 floragunn GmbH
-->

# Integrating with OpenID providers
{: .no_toc}

{% include toc.md %}

Search Guard can integrate with *identity providers* (IdP) that are compatible with the OpenID standard. This will allow for:

* Automatic configuration
  * You just need to point Search Guard to the *metadata* of your IdP, and Search Guard will use that data for configuration
* Automatic key fetching
  * Search Guard will retrieve the public key for validating the JSON web tokens automatically from the JWKS endpoint of your IdP. There is no need to configure keys or shared secrets in `sg_config.yml`.
* Key rollover
  * You can change the keys used for signing the JWTs directly in your IdP. If Search Guard detects an unknown key, it tries to retrieve it from the IdP, transparent to the user.
* [Kibana Single Sign On](../_docs_kibana/kibana_authentication_openid.md)
   
## Identity providers

Identity Provider (IdP) is a third party that confirms Client's identity. One would use ann IdP in the following cases: you want to log into an application to book your hotel , but in order to save time instead of creating a user profile from scratch you click on log in with Facebook. This is the basic idea behind IdP. OAuth 2.0 is a protocol, which identity providers such as Facebook will use to issue Access Token which are send from the Authorization Server to the Client Application (hotel booking website) Thanks to the Access Token an Authorization is taking place and the resource such as hotel website can be accessed on behalf of the user. The Access Token is issued only for a limited amount of time. 

![](./IdConnect/Asset_1.jpg)

Nevertheless the OAuth 2.0 itself is not sufficient, as it is responsible solely for the Authorization. This is where OpenId Connect comes into picture. The OpenId Connect provides the identity layer on top of the OAuth 2.0 . The role of the OpenId Connect is to provide the authentication , which OAuth 2.0 is not capable of. OAuth 2.0 grants a temporary access to the Application and there is no identity implication in the process. The OpenId Connect is needed in order to provide details about the transaction, such as when did the transaction took place, what happened ect. The OpenId Connect is also used to allow the Single-Sign-On. A sample OpenId Connect flow would consist of the end user being authenticated using the Identity Provider. The Provider would issue an ID Token to the Relying Party. An ID Token has a form of a JSON Web Token . The payload contains claims and scopes about the end user  The Identity provider interacts with many endpoints such as Authorization endpoint, Token endpoint and user Info endpoint. 

![](./IdConnect/Asset_2.jpg)

In the context of the OpenID Connect the Authorization, Token and User Info endpoints. The role of the Authorization endpoint is to grant the access to the ID Token to the Client. The Token endpoint on the other hand is responsible for the exchange of the information between the Authorization endpoint and the ID Token. Finally, the user Info endpoint contains profile information. 

![](./IdConnect/Asset_3.jpg)

SearchGuard can integrate with Identity Providers which are compatible with the OpenID standard. This will allow for: Automatic configuration (You need to point SearchGuard to the metadata of your IdPand SearchGuard will use the data for configuration), Automatic Key Fetching (SearchGuard will retrive the public key for validating the JSON Web token automatically from the JWKS endpoint of your IdP. There is no need to configure keys or shared secrets in sg_config.yml),  Key rollover (You can change the keys used for signing the JWTs directly in your IdP if SearchGuard detects an unknown key , it tries to retrive it  from the IdP transparent to the user) and Kibana Single Sign-On.

![](./IdConnect/Asset_4.jpg)


## Configuring OpenID integration

To integrate with an OpenID IdP, set up an authentication domain and choose `openid` as HTTP authentication type. Since JSON web tokens already contain all required information to verify the request, `challenge` can be set to `false` and `authentication_backend` to `noop`.

Minimal configuration:

```yaml
openid_auth_domain:
  http_enabled: true
  transport_enabled: true
  order: 0
  http_authenticator:
    type: openid
    challenge: false
    config:
      subject_key: preferred_username
      roles_key: roles
      openid_connect_url: https://keycloak.example.com:8080/auth/realms/master/.well-known/openid-configuration
  authentication_backend:
    type: noop
```

Configuration parameters:

![](./IdConnect/Asset_5.jpg)

## OpenID connect URL

OpenID specifies various endpoints for integration purposes. The most important endpoint is the `well-known` configuration endpoint: It lists endpoints and other configuration options relevant to Search Guard.

The URL differs between IdPs, but it usually ends in `/.well-known/openid-configuration`. Keycloak example:

```
http(s)://<server>:<port>/auth/realms/<realm>/.well-known/openid-configuration
```

The main information that Search Guard needs is the `jwks_uri`. This URI specifies where the IdP's public key(s) in JWKS format can be found. Example:

```
jwks_uri: "https://keycloak.example.com:8080/auth/realms/master/protocol/openid-connect/certs"
```

```
{  
   keys:[  
      {  
         kid:"V-diposfUJIk5jDBFi_QRouiVinG5PowskcSWy5EuCo",
         kty:"RSA",
         alg:"RS256",
         use:"sig",
         n:"rI8aUrAcI_auAdF10KUopDOmEFa4qlUUaNoTER90XXWADtKne6VsYoD3ZnHGFXvPkRAQLM5d65ScBzWungcbLwZGWtWf5T2NzQj0wDyquMRwwIAsFDFtAZWkXRfXeXrFY0irYUS9rIJDafyMRvBbSz1FwWG7RTQkILkwiC4B8W1KdS5d9EZ8JPhrXvPMvW509g0GhLlkBSbPBeRSUlAS2Kk6nY5i3m6fi1H9CP3Y_X-TzOjOTsxQA_1pdP5uubXPUh5YfJihXcgewO9XXiqGDuQn6wZ3hrF6HTlhNWGcSyQPKh1gEcmXWQlRENZMvYET-BuJEE7eKyM5vRhjNoYR3w",
         e:"AQAB"
      }
   ]
}
```

You can find more information about the endpoint for your IdP here:

* [Okta](https://developer.okta.com/docs/api/resources/oidc#well-knownopenid-configuration){:target="_blank"}
* [Kecloak](https://www.keycloak.org/docs/3.0/securing_apps/topics/oidc/oidc-generic.html){:target="_blank"}
* [Auth0](https://auth0.com/docs/protocols/oidc/openid-connect-discovery){:target="_blank"}
* [Connect2ID](https://connect2id.com/products/server/docs/api/discovery){:target="_blank"}
* [Salesforce](https://help.salesforce.com/articleView?id=remoteaccess_using_openid_discovery_endpoint.htm&type=5){:target="_blank"}
* [IBM OpenID connect](https://www.ibm.com/support/knowledgecenter/en/SSEQTP_8.5.5/com.ibm.websphere.wlp.doc/ae/rwlp_oidc_endpoint_urls.html){:target="_blank"}

## Fetching public keys: The key id

When an IdP generates and signs a JSON web token, it must add the id of the key to the *header* part of the JWT. Example:

```
{
  "alg": "RS256",
  "typ": "JWT",
  "kid": "V-diposfUJIk5jDBFi_QRouiVinG5PowskcSWy5EuCo"
}
```

As per [OpenID specification](http://openid.net/specs/openid-connect-messages-1_0-20.html){:target="_blank"}, the `kid` ("key id") is mandatory. Token verification will not work if an IdP fails to add the `kid` field to the JWT. 

If Search Guard receives a JWT with an unknown `kid`, it will visit the IdP's `jwks_uri` and retrieve all currently available valid keys. These keys will be used and cached until a refresh is triggered by retrieving another unknown key id.

## Key rollover and multiple public keys

Search Guard is capable of maintaining multiple valid public keys at once. Since the OpenID specification does not allow for a validity period of public keys, a key is deemed valid until it has been removed from the list of valid keys in your IdP, and until the list of valid keys has been refreshed.

If you want to roll over a key in your IdP, it is good practice to:

* Create a new key pair in your IdP, and give the new key a higher priority than the currently used key
* Your IdP will use this new key over the old key
* Upon first appearance of the new `kid` in a JWT, Search Guard will refresh the key list 
  * At this point, both the old key and the new key are valid
  * Tokens signed with the old key are also still valid 
* The old key can be removed from your IdP when the last JWT signed with this key has timed out

If you have to immediately change your public key, because the currently used key has been comprised, you can also delete the old key first and then create a new one. In this case, all JWTs signed with the old key will become invalid immediately.

## TLS settings

In order to prevent man-in-the-middle attacks and other attack scenarios you should secure the connection between Search Guard and your IdP with TLS.

### Enabling TLS

Use the following parameters to enable TLS for connecting to your IdP:

```yaml
config:
  enable_ssl: <true|false>
  verify_hostnames: <true|false>
```

![](./IdConnect/Asset_6.jpg)

### Certificate validation

To validate the TLS certificate of your IdP, configure either the path to the IdP's root CA or the root certificates content like:

```yaml
config:
  pemtrustedcas_filepath: /path/to/trusted_cas.pem
```

or

```yaml
config:
  pemtrustedcas_content: |-
    MIID/jCCAuagAwIBAgIBATANBgkqhkiG9w0BAQUFADCBjzETMBEGCgmSJomT8ixk
    ARkWA2NvbTEXMBUGCgmSJomT8ixkARkWB2V4YW1wbGUxGTAXBgNVBAoMEEV4YW1w
    bGUgQ29tIEluYy4xITAfBgNVBAsMGEV4YW1wbGUgQ29tIEluYy4gUm9vdCBDQTEh
    ...
```


![](./IdConnect/Asset_7.jpg)


### TLS client authentication

To use TLS client authentication, configure the PEM certificate and private key Search Guard should send for TLS client authentication, or its contents like:

```yaml
config:
  pemkey_filepath: /path/to/private.key.pem
  pemkey_password: private_key_password
  pemcert_filepath: /path/to/certificate.pem
```

or

```yaml
config:
  pemkey_content: |-
    MIID2jCCAsKgAwIBAgIBBTANBgkqhkiG9w0BAQUFADCBlTETMBEGCgmSJomT8ixk
    ARkWA2NvbTEXMBUGCgmSJomT8ixkARkWB2V4YW1wbGUxGTAXBgNVBAoMEEV4YW1w
    bGUgQ29tIEluYy4xJDAiBgNVBAsMG0V4YW1wbGUgQ29tIEluYy4gU2lnbmluZyBD
    ...
  pemkey_password: private_key_password
  pemcert_content: |-
    MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCHRZwzwGlP2FvL
    oEzNeDu2XnOF+ram7rWPT6fxI+JJr3SDz1mSzixTeHq82P5A7RLdMULfQFMfQPfr
    WXgB4qfisuDSt+CPocZRfUqqhGlMG2l8LgJMr58tn0AHvauvNTeiGlyXy0ShxHbD
    ...
```

![](./IdConnect/Asset_8.jpg)

### Enabled ciphers and protocols

You can limit the allowed ciphers and TLS protocols by using the following keys:

![](./IdConnect/Asset_9.jpg)


## Expert: DOS protection

In theory it is possible to DOS attack an OpenID based infrastructure by sending tokens with randomly generated, non-existing key ids at a high frequency. In order to mitigate this, Search Guard will only allow a maximum number of new key ids in a certain time frame. If more unknown key ids are receievd, Search Guard will return a HTTP status code 503 (Service not available) and refuse to query the IdP. By defaut, Search Guard does not allow for more than 10 unknown key ids in a time window of 10 seconds. You can control these settings by the following configuration keys: 

![](./IdConnect/Asset_10.jpg)

## Kibana Single Sign On

The Kibana Plugin has OpenID support built in since version 14. Please refer to the [Kibana OpenID configuration](../_docs_kibana/kibana_authentication_openid.md) for details.