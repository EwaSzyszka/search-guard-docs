---
title: SAML Authentication
html_title: SAML Authentication
slug: saml-authentication
category: authauth
order: 700
layout: docs
edition: enterprise
description: How to configure SAML support in Search Guard to implement Kibana Single Sign On.
resources:
  - "https://search-guard.com/kibana-elasticsearch-saml/|Using SAML for Kibana Single Sign-On (blogpost)"

---
<!---
Copyright 2019 floragunn GmbH
-->

# SAML Authentication
{: .no_toc}

{% include toc.md %}

Security Assertion Markup Language (SAML) is a standard that enables security information to be shared between various parties. 
 
 In a typical SAML transaction there are three model roles such as Participant (he gets authenticated), Asserting Party (makes assertions about the participant) and Relying Party (receiver of the SAML assertions). A SAML assertion is a small packet of data in XML format that is prepared by the Asserting Party (also called SAML Authority) and it is shipped to the Relying Party. The SAML assertion contains such information as Authentication statements (eg. user was authenticated with user name and password) and tribute statements with more detailed information about the Participant.
 
Using the SAML standard such tasks as Authorization (a process in which we verify if the user has the access to what he is requesting) and Authentification (standard in which it is determined whether the user is the person that he is claiming to be) are achieved. Thanks to SAML the so-called multidomain single sign-on (MSSO) is possible. The MSSO is a special kind of a SAML Assertion, allowing the Participant to access the protected resource (eg. a website) without the need of an additional re-authentication locally, or put in other words you don't have to create a special account on the website you are trying to access, as the Asserting Party verifies your identity and you are allowed to freely browse through a website that is protected.  

![](./SAML/Asset_1.jpg)

Below you can find the use cases of SAML such as separation between identity provider and service provider; the identity provision, identity federation (Authentication delegation to a trusted party such as an Asserting Party), single sign-on.

![](./SAML/Asset_2.jpg)

## Activating SAML

To use SAML for authentication, you need to configure a respective authentication domain in the `authc` section of `sg_config`. Since SAML works solely on the HTTP layer, you do not need any `authentication_backend` and can set it to noop. Place all SAML specific configuration options in this chapter in the `config` section of the SAML HTTP authenticator like:

```yaml
authc:
  saml:
    enabled: true
    order: 1
    http_authenticator:
      type: saml
      challenge: true
      config:
        idp:
          metadata_file: okta.xml
          ...
    authentication_backend:
      type: noop
```

Once you have configured SAML in `sg_config`, you need to also [activate it in Kibana](../_docs_kibana/kibana_authentication_saml.md). 

## Running multiple authentication domains

We recommend adding at least one other authentication domain, such as LDAP or the internal user database, to support API access to Elasticsearch without SAML. For Kibana and the internal Kibana server user, it is also required to add another authentication domain that supports basic authentication. This authentication domain should be placed first in the chain, and the `challenge` flag has to be set to `false`: 

❗❗❗❗❗ There are two challenge fields, this might be confusing to the reader ❗❗❗❗❗


```yaml
authc:
  basic_internal_auth_domain:
    enabled: true
    order: 0
    http_authenticator:
      type: basic
      challenge: false
    authentication_backend:
      type: internal
  saml_auth_domain:
     enabled: true
     order: 1
     http_authenticator:
        type: 'saml'
        challenge: true
        config:
            ...
     authentication_backend:
        type: noop
```


## Identity provider metadata

A SAML IdP provides a SAML 2.0 metadata file describing the IdPs capabilities and configuration. Search Guard can read IdP metadata either from a URL or a file. Which way to choose depends on your IdP and your preferences. The SAML 2.0 metadata file is mandatory.

![](./SAML/Asset_3.jpg)

## Idp and service provider entity ID

An entity ID is a globally unique name for a SAML entity, either an IdP or a Service Provider (SP). The *IdP entity ID* is usually provided by your IdP. The *SP entity ID* is the name of the configured application or client in your IdP. We recommend to add a new application/client for Kibana and use the URL of your Kibana installation as SP entity id.

![](./SAML/Asset_4.jpg)

## Kibana settings

The Web Browser SSO profile works by exchanging information via HTTP GET or POST. For example, after you log in to your IdP, it will issue an HTTP POST back to Kibana containing the `SAML Response`. You need to configure the base URL of your Kibana installation where the HTTP requests are being sent to.

![](./SAML/Asset_5.jpg)

## Username and Role attributes

Subjects (a.k.a usernames) are usually stored in the NameID element of a SAML response:

```
<saml2:Subject>
  <saml2:NameID>admin</saml2:NameID>
  ...
</saml2:Subject>
```

If your IdP is compliant with the SAML 2.0 specification you do not need to set anything special. If your IdP uses a different element name, you can also specify its name explicitly.

Role attributes are optional. However, most IdPs can be configured to add roles in the SAML assertions as well. You can use these roles for [mapping users to Search Guard roles](../_docs_roles_permissions/configuration_roles_mapping.md). Usually, the `Role` element is used for that, e.g.

```
<saml2:Attribute Name='Role'>
  <saml2:AttributeValue >Everyone</saml2:AttributeValue>
  <saml2:AttributeValue >Admins</saml2:AttributeValue>
</saml2:Attribute>
```

If you want to extract roles from the SAML response, you need to specify the element name that contains the roles. 

![](./SAML/Asset_6.jpg)
 
## Request signing

Requests from Search Guard to the IdP can optionally be signed. Use the following settings to configure request signing:

![](./SAML/Asset_7.jpg)

Search Guard supports the following signature algorithms:

![](./SAML/Asset_8.jpg)


## Logout

Usually, IdPs provide information about their individual logout URL in their SAML 2.0 metadata. If this is the case, Search Guard will make use of them and render the correct logout link in Kibana. If your IdP does not support an explicit logout, you can force a re-login when the user visits Kibana again:

![](./SAML/Asset_9.jpg)

At the moment Search Guard only supports the `HTTP-Redirect` logout binding. Please make sure this is configured correctly in your IdP.

## Exchange key settings

SAML, unlike other protocols like JWT or Basic Authentication, is not meant to be used for exchanging user credentials with each request. Therefore Search Guard trades the heavy-weight SAML response for a light-weight JSON web token that stores the validated user attributes. This token is signed by an exchange key that you can choose freely. Note that when you change this key, all tokens signed with it will become invalid immediately.

![](./SAML/Asset_10.jpg)

## TLS settings

If you are loading the IdP metadata from a URL, it is recommended to use SSL/TLS. If you use an external IdP like Okta or Auth0 that uses a trusted certificate, you usually do not need to configure anything. If you host the IdP yourself and use your own root CA, you can customize the TLS settings as follows. These settings are only used for loading SAML metadata over https.

![](./SAML/Asset_11.jpg)

Example:

```yaml
authc:
  saml:
    enabled: true
    order: 1
    http_authenticator:
      type: saml
      challenge: true
      config:
        idp:
          enable_ssl: true
          verify_hostnames: true
          ...
    authentication_backend:
      type: noop
```


### Certificate validation

Configure the root CA used for validating the IdP TLS certificate by setting **one of** the following configuration options:

```yaml
config:
  idp:
    pemtrustedcas_filepath: path/to/trusted_cas.pem
```

or

```yaml
config:
  idp:
    pemtrustedcas_content: |-
      MIID/jCCAuagAwIBAgIBATANBgkqhkiG9w0BAQUFADCBjzETMBEGCgmSJomT8ixk
      ARkWA2NvbTEXMBUGCgmSJomT8ixkARkWB2V4YW1wbGUxGTAXBgNVBAoMEEV4YW1w
      bGUgQ29tIEluYy4xITAfBgNVBAsMGEV4YW1wbGUgQ29tIEluYy4gUm9vdCBDQTEh
      ...
```

![](./SAML/Asset_12.jpg)

### Client authentication

Search Guard can use TLS client authentication when fetching the IdP metadata. If enabled, Search Guard sends a TLS client certificate to the IdP for each metadata request. Use the following keys to configure client authentication:

![](./SAML/Asset_13.jpg)

You can limit the allowed cipher and TLS protocols for the IdP connection. For example you can only enable strong cipher and limit the TLS versions to the most recent ones.

![](./SAML/Asset_14.jpg)


**Note: By default, Search Guard disables `TLSv1` because it is insecure. If you need to use `TLSv1` and you know what you  are doing, you can re-enable it like:**

```yaml
enabled_ssl_protocols:
  - "TLSv1"
  - "TLSv1.1"
  - "TLSv1.2"
```

## Minimal configuration example

```yaml
authc:
  saml:
    enabled: true
    order: 1
    http_authenticator:
      type: saml
      challenge: true
      config:
        idp:
          metadata_file: metadata.xml
          entity_id: http://idp.example.com/
        sp:
          entity_id: https://kibana.example.com
        kibana_url: https://kibana.example.com:5601/
        roles_key: Role
        exchange_key: 'peuvgOLrjzuhXf ...'
    authentication_backend:
      type: noop
```

## Kibana configuration

Since most of the SAML specific configuration is done in Search Guard, just activate SAML in your `kibana.yml` by adding:

```
searchguard.auth.type: "saml"
```

In addition the Kibana endpoint for validating the SAML assertions must be whitelisted:

```
server.xsrf.whitelist: ["/searchguard/saml/acs"]
```

If you use the logout POST binding, you also need to whitelist the logout endpoint:

```
server.xsrf.whitelist: ["/searchguard/saml/acs", "/searchguard/saml/logout"]
```

## IdP initated SSO

To use IdP initiated SSO, in your IdP, set the *Assertion Consumer Service* endpoint to:

```
/searchguard/saml/acs/idpinitiated
```

Then add this endpoint to the xsrf whitelist in kibana.yml:

```
server.xsrf.whitelist: ["/searchguard/saml/acs/idpinitiated", "/searchguard/saml/acs", "/searchguard/saml/logout"]
```